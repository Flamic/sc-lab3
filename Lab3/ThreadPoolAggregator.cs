﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lab3_1
{
    public static class ThreadPoolAggregator
    {
        public static int Sum(
            int[] arr,
            int threadsCount)
        {
            var arrays = arr.Split(arr.Length / threadsCount).ToArray();
            var res = Enumerable.Repeat(int.MinValue, threadsCount).ToArray();
            for (var i = 0; i < threadsCount; ++i)
            {
                ThreadPool.QueueUserWorkItem(
                    (state) => res[(int)state] = arrays[(int)state].Sum(), i
                );
            }
            
            for (var i = 0; i < res.Length; ++i)
                while (res[i] == int.MinValue) {}

            return res.Sum();
        }

        public static double Average(
            int[] arr,
            int threadsCount)
        {
            var arrays = arr.Split(arr.Length / threadsCount).ToArray();
            var res = Enumerable.Repeat(double.NegativeInfinity, threadsCount).ToArray();
            for (var i = 0; i < threadsCount; ++i)
            {
                ThreadPool.QueueUserWorkItem(
                    (state) => res[(int)state] = arrays[(int)state].Sum(), i
                );
            }

            for (var i = 0; i < res.Length; ++i)
                while (double.IsNegativeInfinity(res[i])) { }

            return res.Sum() / arr.Length;
        }

        public static int Mode(
            int[] arr,
            int threadsCount,
            int minVal = int.MinValue,
            int maxVal = int.MaxValue)
        {
            var arrays = arr.Split(arr.Length / threadsCount).ToArray();
            var res = new int[maxVal - minVal + 1];
            var results = new int[threadsCount][];
            var isReady = new bool[maxVal - minVal + 1];
            for (var i = 0; i < arrays.Length; ++i)
            {
                results[i] = new int[maxVal - minVal + 1];
                ThreadPool.QueueUserWorkItem((state) =>
                {
                    foreach (var p in arrays[(int)state])
                        ++results[(int) state][p - minVal];
                    isReady[(int) state] = true;
                }, i);
            }

            for (var i = 0; i < arrays.Length; ++i)
            {
                while (!isReady[i]) {}
                res = res.Merge(results[i]);
            }

            return res.MaxIndex() + minVal;
        }
    }
}

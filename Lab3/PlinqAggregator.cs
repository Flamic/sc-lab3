﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_1
{
    public static class PlinqAggregator
    {
        public static int Sum(
            int[] arr,
            int threadsCount)
        {
            return arr
                .AsParallel()
                .WithDegreeOfParallelism(threadsCount)
                .Sum();
        }

        public static double Average(
            int[] arr,
            int threadsCount)
        {
            return arr
                .AsParallel()
                .WithDegreeOfParallelism(threadsCount)
                .Average();
        }

        public static int Mode(
            int[] arr,
            int threadsCount)
        {
            return arr
                .AsParallel()
                .WithDegreeOfParallelism(threadsCount)
                .GroupBy(value => value)
                .OrderByDescending(group => group.Count())
                .Select(group => group.Key)
                .First(); ;
        }
    }
}

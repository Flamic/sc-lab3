﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_1
{
    public static class EnumerableExtension
    {
        /// <summary>
        /// Splits an array into several smaller arrays.
        /// </summary>
        /// <typeparam name="T">The type of the array.</typeparam>
        /// <param name="array">The array to split.</param>
        /// <param name="size">The size of the smaller arrays.</param>
        /// <returns>An array containing smaller arrays.</returns>
        public static IEnumerable<IEnumerable<T>> Split<T>(this T[] array, int size)
        {
            for (var i = 0; i < (float)array.Length / size; i++)
            {
                yield return array.Skip(i * size).Take(size);
            }
        }

        public static int[] Merge(this int[] arr1, int[] arr2)
        {
            return arr1.Zip(arr2, (a1, a2) => a1 + a2).ToArray();
        }

        public static int MaxIndex(this int[] arr)
        {
            var max = int.MinValue;
            var maxIndex = 0;
            for (var i = 0; i < arr.Length; i++)
            {
                if (arr[i] <= max) continue;
                max = arr[i];
                maxIndex = i;
            }

            return maxIndex;
        }
    }
}

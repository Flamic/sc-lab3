﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_1
{
    class Program
    {
        public struct TestResult<T>
        {
            public T Result { get; set; }
            public TimeSpan ElapsedTime { get; set; }
        }

        private static readonly int[] THREADS_COUNT = { 1, 2, 4, 10 };
        private static readonly int ELEMENTS_COUNT = 1000000;
        private static readonly int MIN_VALUE = 1;
        private static readonly int MAX_VALUE = 50;

        public static void Main(string[] args)
        {
            var rand = new Random();
            var arr = new int[ELEMENTS_COUNT];
            for (var i = 0; i < ELEMENTS_COUNT; ++i)
            {
                arr[i] = rand.Next(MIN_VALUE, MAX_VALUE + 1);
            }
            
            Console.WriteLine("Task (async, await):");
            PrintResult(Test(i => TaskAggregator.Sum(arr, i)).ToList(), "Sum");
            PrintResult(Test(i => TaskAggregator.Average(arr, i)).ToList(), "Average");
            PrintResult(Test(i => TaskAggregator.Mode(arr, i, MIN_VALUE, MAX_VALUE)).ToList(), "Mode");

            Console.WriteLine("\nPLINQ:");
            PrintResult(Test(i => PlinqAggregator.Sum(arr, i)).ToList(), "Sum");
            PrintResult(Test(i => PlinqAggregator.Average(arr, i)).ToList(), "Average");
            PrintResult(Test(i => PlinqAggregator.Mode(arr, i)).ToList(), "Mode");

            Console.WriteLine("\nThread pool:");
            PrintResult(Test(i => ThreadPoolAggregator.Sum(arr, i)).ToList(), "Sum");
            PrintResult(Test(i => ThreadPoolAggregator.Average(arr, i)).ToList(), "Average");
            PrintResult(Test(i => ThreadPoolAggregator.Mode(arr, i, MIN_VALUE, MAX_VALUE)).ToList(), "Mode");

            Console.WriteLine("\nParallel:");
            PrintResult(Test(i => ParallelAggregator.Sum(arr, i)).ToList(), "Sum");
            PrintResult(Test(i => ParallelAggregator.Average(arr, i)).ToList(), "Average");
            PrintResult(Test(i => ParallelAggregator.Mode(arr, i, MIN_VALUE, MAX_VALUE)).ToList(), "Mode");

            Console.ReadLine();
        }
        
        private static void PrintResult<T>(IReadOnlyList<TestResult<T>> results, string description)
        {
            for (var i = 0; i < THREADS_COUNT.Length; ++i)
            {
                Console.WriteLine($"{description}: {results[i].Result} // " +
                                  $"{THREADS_COUNT[i]} thread(s): {results[i].ElapsedTime.ToStringTime()}");
            }
            Console.WriteLine(new string('-', 15));
        }

        private static IEnumerable<TestResult<T>> Test<T>(Func<int, T> testFunc)
        {
            var stopwatch = new Stopwatch();
            var res = new TestResult<T>[THREADS_COUNT.Length];
            for (var i = 0; i < THREADS_COUNT.Length; ++i)
            {
                stopwatch.Restart();
                res[i].Result = testFunc(THREADS_COUNT[i]);
                stopwatch.Stop();
                res[i].ElapsedTime = stopwatch.Elapsed;
            }

            return res;
        }
    }
}

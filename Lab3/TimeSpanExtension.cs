﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_1
{
    public static class TimeSpanExtension
    {
        public static string ToStringTime(this TimeSpan ts)
        {
            return $"{ts.TotalMilliseconds} ms";
        }
    }
}

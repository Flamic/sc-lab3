﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_1
{
    public static class ParallelAggregator
    {
        public static int Sum(
            int[] arr,
            int threadsCount)
        {
            var arrays = arr.Split(arr.Length / threadsCount).ToArray();
            var res = new int[threadsCount];

            var result =
                Parallel.For(0, threadsCount, (i) => res[i] = arrays[i].Sum());
            
            while (!result.IsCompleted) {}

            return res.Sum();
        }

        public static double Average(
            int[] arr,
            int threadsCount)
        {
            var arrays = arr.Split(arr.Length / threadsCount).ToArray();
            var res = new double[threadsCount];

            var result =
                Parallel.For(0, threadsCount, (i) => res[i] = arrays[i].Sum());

            while (!result.IsCompleted) {}

            return res.Sum() / arr.Length;
        }

        public static int Mode(
            int[] arr,
            int threadsCount,
            int minVal = int.MinValue,
            int maxVal = int.MaxValue)
        {
            var arrays = arr.Split(arr.Length / threadsCount).ToArray();
            var res = new int[maxVal - minVal + 1];
            var results = new int[threadsCount][];

            var result = Parallel.For(0, threadsCount, (i) =>
            {
                results[i] = new int[maxVal - minVal + 1];
                foreach (var p in arrays[i])
                    ++results[i][p - minVal];
            });

            while (!result.IsCompleted) {}
            res = results.Aggregate(res, (current, i) => current.Merge(i));

            return res.MaxIndex() + minVal;
        }
    }
}

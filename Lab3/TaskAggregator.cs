﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_1
{
    public static class TaskAggregator
    {
        public static async Task<int> SumAsync(IEnumerable<int> arr)
        {
            return await Task.Run(arr.Sum);
        }
        public static int Sum(
            int[] arr,
            int threadsCount)
        {
            var arrays = arr.Split(arr.Length / threadsCount);
            var tasks = arrays.Select(SumAsync).ToList();
            
            return Task.WhenAll(tasks).Result.Sum();
        }

        public static double Average(
            int[] arr,
            int threadsCount)
        {
            return (double)Sum(arr, threadsCount) / arr.Length;
        }

        public static async Task<int[]> ModeAsync(
            IEnumerable<int> arr,
            int minVal = int.MinValue,
            int maxVal = int.MaxValue)
        {
            return await Task.Run(() =>
            {
                var res = new int[maxVal - minVal + 1];
                foreach (var i in arr)
                    ++res[i - minVal];
                return res;
            });
        }

        public static int Mode(
            int[] arr,
            int threadsCount,
            int minVal = int.MinValue,
            int maxVal = int.MaxValue)
        {
            var arrays = arr.Split(arr.Length / threadsCount);
            var tasks = arrays.Select(e => ModeAsync(e, minVal, maxVal)).ToList();
            var results = Task.WhenAll(tasks).Result;
            var res = new int[maxVal - minVal + 1];
            res = results.Aggregate(res, (current, part) => current.Merge(part));

            return res.MaxIndex() + minVal;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_4
{
    class Program
    {
        private static readonly string DATA = new string('#', 15000000);
        static void Main(string[] args)
        {
            while (true)
            {
                Console.Write("Enter file name: ");
                string fileName = Console.ReadLine();
                if (fileName == null) continue;
                WriteToFile(fileName, DATA);
            }
        }

        private static async Task WriteToFile(string fileName, string data)
        {
            try
            {
                using (var sw = new StreamWriter(fileName + ".txt", false, Encoding.Default))
                {
                    await sw.WriteAsync(data);
                }
                ClearCurrentConsoleLine();
                Console.WriteLine($"File {fileName}.txt was successfully created!");
                Console.Write("Enter file name: ");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static void ClearCurrentConsoleLine()
        {
            var currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, currentLineCursor);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
        }
    }
}
